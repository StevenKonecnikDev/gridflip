using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperControl : PlayerControl {


    // Use this for initialization
    new void Awake () {
        base.Awake();
        newTile = occupiedTile;
	}
	
	// Update is called once per frame
	void Update () {

        if (this.animator.GetCurrentAnimatorStateInfo(0).IsName("jump_animation"))
        {
            moving = true;
        }
        else
        {
            animator.SetBool("jumping", false);
            newTile.occupied = true;
        }

        //Set the animator speed to whatever the move speed is
        animator.speed = moveSpeed;

        SetDirection();

        //If not moving then run 'waiting' code
        if (!moving)
        {
            Waiting();
        }
        //If moving then run 'move' code
        if (moving && animator.GetBool("jumping") == false)
        {
            Move();
        }

    }

    public override void Ability()
    {
        newTile = occupiedTile;
        newTile.occupied = true;
        newPosition = newTile.transform.position;
        newPosition.y += lift;
        animator.SetBool("jumping", true);
    }
}
