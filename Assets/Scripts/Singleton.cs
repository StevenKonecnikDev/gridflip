using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour {

    //static reference of instance
    public static Singleton instance;

	// Use this for initialization
	void Awake () {

        //set instance to reference this object and don't destroy on load
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        //if instance has already been set to an object then destroy this one
        else
        {
            Destroy(gameObject);
        }
	}
}
