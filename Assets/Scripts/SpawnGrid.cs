﻿using UnityEngine;
using System.Collections;

public class SpawnGrid : MonoBehaviour {
	
	public GameObject spawnObject;
	public int numberOfObjectsX = 1;
	public int numberOfObjectsZ = 1;
	
	// Use this for initialization
	void Start () {
	
		float cameraObjectYDifference = transform.position.y - Camera.main.transform.position.y;
		
		transform.position = Camera.main.ScreenToWorldPoint(new Vector3(0,cameraObjectYDifference,0));
	
	for (int i = 0; i < numberOfObjectsX; i++)
		{
			for (int j = 0; j < numberOfObjectsZ; j++)
			{
				Vector3 spawnPosition = transform.position;
				spawnPosition.x += spawnObject.transform.localScale.x/2;
				spawnPosition.z += spawnObject.transform.localScale.z/2;
				spawnPosition.y -= (numberOfObjectsX + numberOfObjectsZ)/2;
				if (i%2 != 0)//is odd
					spawnPosition.x -= (i+1)/2;
				if (i%2 == 0)//is even
					spawnPosition.x += i/2;
				if (j%2 != 0)//is odd
					spawnPosition.z -= (j+1)/2;
				if (j%2 == 0)//is even
					spawnPosition.z += j/2;
				GameObject newObject = Instantiate(spawnObject, spawnPosition, spawnObject.transform.rotation)as GameObject;
				newObject.transform.parent = transform;
			}
			
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnDrawGizmos() {		
		for (int i = 0; i < numberOfObjectsX; i++)
		{
			for (int j = 0; j < numberOfObjectsZ; j++)
			{
				Vector3 spawnPosition = transform.position;
				spawnPosition.x += spawnObject.transform.localScale.x/2;
				spawnPosition.z += spawnObject.transform.localScale.z/2;
				spawnPosition.y -= (numberOfObjectsX + numberOfObjectsZ)/2;
				if (i%2 != 0)//is odd
					spawnPosition.x -= (i+1)/2;
				if (i%2 == 0)//is even
					spawnPosition.x += i/2;
				if (j%2 != 0)//is odd
					spawnPosition.z -= (j+1)/2;
				if (j%2 == 0)//is even
					spawnPosition.z += j/2;
				Gizmos.DrawLine(spawnPosition + Vector3.left, spawnPosition + Vector3.right);
				Gizmos.DrawLine(spawnPosition + Vector3.up, spawnPosition + Vector3.down);
				Gizmos.DrawLine(spawnPosition + Vector3.forward, spawnPosition + Vector3.back);
			}
			
		}
	}
}