using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClonerControl : PlayerControl {

    public bool cloned = false;
    private ClonerControl cloneControl;
    private GameObject thisClone;
    private GameObject otherClone;
    private GameObject clone;
    private Color transparent;


    // Use this for initialization
    new void Awake () {
        base.Awake();
        thisClone = gameObject;
    }
	
	// Update is called once per frame
	void Update () {

        

        //Set the animator speed to whatever the move speed is
        animator.speed = moveSpeed;

        SetDirection();

        //If not moving then run 'waiting' code
        if (!moving)
        {
            Waiting();
        }
        //If moving then run 'move' code
        if (moving)
        {
            Move();
        }

    }

    public override void Ability()
    {
        //Destroy the cloned player
        if (cloned)
        {
            otherClone.GetComponent<PlayerControl>().enabled = true;
            cloned = false;
            occupiedTile.OnTriggerExit();
            Destroy(gameObject);
        }
        //Clone the player
        else if (!cloned)
        {
            clone = (GameObject)Instantiate(thisClone, transform.position, transform.rotation);
            thisClone.GetComponent<PlayerControl>().enabled = false;
            cloneControl = clone.GetComponent<ClonerControl>();
            cloneControl.otherClone = thisClone;
            cloneControl.cloned = true;
            //Make clone transparent
            transparent = clone.transform.GetChild(0).GetComponent<Renderer>().material.color;
            transparent.a = 0.5f;
            clone.transform.GetChild(0).GetComponent<Renderer>().material.color = transparent;
            //Disable the collider for the first movement
            Collider collider = clone.transform.GetChild(0).GetComponent<Collider>();
            collider.enabled = false;
        }
        
    }

    //Overriding main PlayerControl move function
    public override void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, newPosition, moveSpeed * Time.deltaTime);
        //Once player has reached the new positon
        if (transform.position == newPosition)
        {
            if (dying)
            {
                Destroy(gameObject);
            }

            //Re-enabling collider
            Collider collider = gameObject.transform.GetChild(0).GetComponent<Collider>();
            if (collider.enabled == false)
            {
                collider.enabled = true;
            }
            else
            {
                //release previously occupied tile
                oldTile = occupiedTile;
                oldTile.occupied = false;
            }
            //set new tile to be occupied
            occupiedTile = newTile;
            occupiedTile.occupied = true;

            //Return to Ready state
            animator.SetBool("hopping", false);
            animator.SetBool("leaping", false);
            moving = false;
        }
        return;
    }
}
