using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleFlip : MonoBehaviour {

    public string flipDirection = "stationary";
    public float flipSpeed = 6.0f;

    private bool flipping = false;
    private Material defaultMaterial;
    private Material player1Material;
    private Material player2Material;

    // Use this for initialization
    void Start () {

        player1Material = Resources.Load("Materials/Player1Tile", typeof(Material)) as Material;
        player2Material = Resources.Load("Materials/Player2Tile", typeof(Material)) as Material;
        defaultMaterial = Resources.Load("Materials/DefaultColor", typeof(Material)) as Material;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseOver ()
    {

        if (Input.GetMouseButtonDown(0))
        {
            foreach (Transform child in transform)
            {
                if (child.position.y > transform.position.y)
                {
                    child.GetComponent<Renderer>().material = player1Material;
                }
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            foreach (Transform child in transform)
            {
                if (child.position.y > transform.position.y)
                {
                    child.GetComponent<Renderer>().material = player2Material;
                }
            }
        }
        if (Input.GetMouseButtonDown(2))
        {
            foreach (Transform child in transform)
            {
                if (child.position.y > transform.position.y)
                {
                    child.GetComponent<Renderer>().material = defaultMaterial;
                }
            }
        }


        if (!flipping)
        {
            flipping = true;
            StartCoroutine(Flip());
        }

    }


    IEnumerator Flip()
    {

        if (flipDirection == "up")
        {
            transform.Rotate(flipSpeed, 0, 0, Space.World);
            while (Flipping())
            {
                transform.Rotate(flipSpeed, 0, 0, Space.World);
                yield return null;
            }
        }
        if (flipDirection == "left")
        {
            transform.Rotate(0, 0, flipSpeed, Space.World);
            while (Flipping())
            {
                transform.Rotate(0, 0, flipSpeed, Space.World);
                yield return null;
            }
        }
        if (flipDirection == "down")
        {
            transform.Rotate(-flipSpeed, 0, 0, Space.World);
            while (Flipping())
            {
                transform.Rotate(-flipSpeed, 0, 0, Space.World);
                yield return null;
            }
        }
        if (flipDirection == "right")
        {
            transform.Rotate(0, 0, -flipSpeed, Space.World);
            while (Flipping())
            {
                transform.Rotate(0, 0, -flipSpeed, Space.World);
                yield return null;
            }
        }
    }

    //Check if tile is still flipping (hasn't reached a point where it can stop yet)
    bool Flipping()
    {

        if (transform.rotation.x != 1 && transform.rotation.x != -1 && transform.rotation.y != 1 && transform.rotation.y != -1 && transform.rotation.z != 1 && transform.rotation.z != -1 && transform.rotation.w != 1 && transform.rotation.w != -1)
        {
            return true;
        }
        else {
            flipping = false;
            return false;
        }
    }
}
