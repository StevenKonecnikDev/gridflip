using UnityEngine;
using System.Collections;
using Rewired;

public class PlayerControl : MonoBehaviour {

    //========
    // Globals
    //========

    //Public Variables
    public int playerId;
    public string direction = "up";
	public int moveSpeed = 5;
	public float score = 0.0f;
	public int percent = 0;
    public bool ready = false;
    public bool killMe;
    public bool canTurn = true;
    public bool onTop = true;


    //Private Variables
    protected Player player;
    protected Vector3 playerInitialPosition;
    protected Vector3 newPosition;
    protected Animator animator;

    protected string newDirection = "up";

    protected float lift = 0.1f;

    protected bool moving;
    protected bool dying;

    public FlipTile occupiedTile;
    public FlipTile oldTile;
    public FlipTile newTile;
    private FlipTile upTile;
	private FlipTile leftTile;
	private FlipTile downTile;
	private FlipTile rightTile;

    private GameManager gameManager;

    //===============
    // Initialization
    //===============

    public void Awake()
    {
        //Get a reference to the GameManager
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        animator = GetComponent<Animator>();

        //Set this player's name based on playerId from rewired
        if (playerId == 0)
        {
            transform.name = "Player1";
        }
        if (playerId == 1)
        {
            transform.name = "Player2";
        }
        if (playerId == 2)
        {
            transform.name = "Player3";
        }
        if (playerId == 3)
        {
            transform.name = "Player4";
        }

        //Get a reference to the player from rewired
        player = ReInput.players.GetPlayer(playerId);

        //Grab a reference to the closest tile and set as occupied
        occupiedTile = FindOccupiedTile().GetComponent<FlipTile>();
        occupiedTile.occupied = true;

        //Set player's speed
        moveSpeed = gameManager.playerSpeed;

        if (!onTop)
        {

        }
        else
        {
            //Define player's initial position
            playerInitialPosition = occupiedTile.transform.position;
            playerInitialPosition.y += lift;

            //Move player to initial position
            if (transform.position != playerInitialPosition)
            {
                newTile = occupiedTile;
                newPosition = playerInitialPosition;
                moving = true;
            }
        }
    }

	//-----------------
	// Main Update Loop
	//-----------------

	void Update () {

        animator.speed = moveSpeed;

        SetDirection();

        //If not moving then run 'waiting' code
        if (!moving){
			Waiting();
		}
        //If moving then run 'move' code
		if (moving){
            Move();
		}
        
    }

    //While player is waiting options are..
    public void Waiting()
    {
        //stop any animation of the player
        animator.SetBool("hopping", false);
        animator.SetBool("leaping", false);

        //Turn the player
        if (canTurn)
        {
            Turn();
        }

        //Hop to the tile the player is looking at
        if (player.GetButton("Hop"))
        {
            Hop();
        }
        //Leap over one tile in the direction the player is looking
        else if (player.GetButton("Leap"))
        {
            Leap();
        }
        //Activate the player's ability
        else if (player.GetButtonDown("Ability") && gameManager.abilitiesActivated)
        {
            Ability();
        }
        //Left Bumper
        else if (player.GetButtonDown("Left Bumper") && gameManager.abilitiesActivated)
        {
            CloneAbility();
        }
        //Right Bumper
        else if (player.GetButtonDown("Right Bumper") && gameManager.abilitiesActivated)
        {
            SwapAbility();
        }

        //Set player to be ready
        if (player.GetButtonDown("Start"))
        {
            Ready();
        }
        //Destroy the player
        if (killMe)
        {
            //newTile = oldTile;
            newPosition = transform.position;
            newPosition.y += 2.0f;
            dying = true;
            moving = true;
        }

    }

    // Set a new direction based on player input
    public void SetDirection()
    {
        // player presses up
        if (player.GetAxis("Move Vertical") > Mathf.Abs(player.GetAxis("Move Horizontal")))
        {
            newDirection = "up";
        }
        //player presses left
        if (player.GetAxis("Move Horizontal") < Mathf.Abs(player.GetAxis("Move Vertical")) * -1)
        {
            newDirection = "left";
        }
        //player presses down
        if (player.GetAxis("Move Vertical") < Mathf.Abs(player.GetAxis("Move Horizontal")) * -1)
        {
            newDirection = "down";
        }
        //player presses right
        if (player.GetAxis("Move Horizontal") > Mathf.Abs(player.GetAxis("Move Vertical")))
        {
            newDirection = "right";
        }
        return;
    }

    //---------
    //Turn
    //---------
    public void Turn()
    {
        direction = newDirection;
        // look at direction
        if (direction == "up")
        {
            newPosition = transform.position;
            newPosition.z += 1;
            transform.LookAt(newPosition);
            return;
        }
        if (direction == "left")
        {
            newPosition = transform.position;
            newPosition.x -= 1;
            transform.LookAt(newPosition);
            return;
        }
        if (direction == "down")
        {
            newPosition = transform.position;
            newPosition.z -= 1;
            transform.LookAt(newPosition);
            return;
        }
        if (direction == "right")
        {
            newPosition = transform.position;
            newPosition.x += 1;
            transform.LookAt(newPosition);
            return;
        }
    }

    //---------
    //Move
    //---------
    public virtual void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, newPosition, moveSpeed * Time.deltaTime);
        //Once player has reached the new positon
        if (transform.position == newPosition)
        {
            //Re-enabling collider
            Collider collider = gameObject.transform.GetChild(0).GetComponent<Collider>();
            if (collider.enabled == false)
            {
                collider.enabled = true;
            }
            else
            {
                //release previously occupied tile
                oldTile = occupiedTile;
                oldTile.occupied = false;
            }
            if (dying)
            {
                Destroy(gameObject);
            }
            else
            {
                //set new tile to be occupied
                occupiedTile = newTile;
                occupiedTile.occupied = true;
            }

            //Return to Ready state
            animator.SetBool("hopping", false);
            animator.SetBool("leaping", false);
            moving = false;
        }
		return;
	}

    //---------
    //Hop
    //---------
    public virtual void Hop()
    {
        switch (direction)
        {
        case "up":
            //if there is a tile above the occupied tile
            if (occupiedTile.upTile)
            {
                //if the tile is not currently flipping
                if (occupiedTile.upTile.isRunning == false)
                {
                    //if the tile above is unoccupied
                    if (occupiedTile.upTile.occupied == false)
                    {
                        //new tile becomes occupied
                        newTile = occupiedTile.upTile;
                        newTile.occupied = true;
                        newPosition = newTile.transform.position;
                        newPosition.y += lift;
                        //play hop animation
                        animator.SetBool("hopping", true);
                        //go to "moving" state
                        moving = true;
                    }
                }
            }
            break;

        case "left":
            //if there is a tile to the left of the occupied tile
            if (occupiedTile.leftTile)
            {
                //if the tile is not currently flipping
                if (occupiedTile.leftTile.isRunning == false)
                {
                    //if the tile to the left is unoccupied
                    if (occupiedTile.leftTile.occupied ==false)
                    {
                        //new tile becomes occupied
                        newTile = occupiedTile.leftTile;
                        newTile.occupied = true;
                        newPosition = newTile.transform.position;
                        newPosition.y += lift;
                        //play hop animation
                        animator.SetBool("hopping", true);
                        //go to "moving" state
                        moving = true;
                    }
                }
            }
            break;

        case "down":
            
            //if there is a tile below the occupied tile
            if (occupiedTile.downTile)
            {
                //if the tile is not currently flipping
                if (occupiedTile.downTile.isRunning == false)
                {
                    //if the tile below is unoccupied
                    if (occupiedTile.downTile.occupied == false)
                    {
                        //new tile becomes occupied
                        newTile = occupiedTile.downTile;
                        newTile.occupied = true;
                        newPosition = newTile.transform.position;
                        newPosition.y += lift;
                        //play hop animation
                        animator.SetBool("hopping", true);
                        //go to "moving" state
                        moving = true;
                    }
                }
            }
            break;

        case "right":
            
            //if there is a tile to the right of the occupied tile
            if (occupiedTile.rightTile)
            {
                //if the tile is not currently flipping
                if (occupiedTile.rightTile.isRunning == false)
                {
                    //if the tile to the right is unoccupied
                    if (occupiedTile.rightTile.occupied == false)
                    {
                        //new tile becomes occupied
                        newTile = occupiedTile.rightTile;
                        newTile.occupied = true;
                        newPosition = newTile.transform.position;
                        newPosition.y += lift;
                        //play hop animation
                        animator.SetBool("hopping", true);
                        //go to "moving" state
                        moving = true;
                    }
                }
            }
            break;
        }
    }

    //---------
    //Leap
    //---------
    public void Leap()
    {
        switch (direction)
        {
            case "up":
                if (occupiedTile.upTile)
                {
                    //if there is a tile above the occupied tile
                    if (occupiedTile.upTile.upTile)
                    {
                        //if the tile is not currently flipping
                        if (occupiedTile.upTile.upTile.isRunning == false)
                        {
                            //if the tile above is unoccupied
                            if (occupiedTile.upTile.upTile.occupied == false)
                            {
                                //new tile becomes occupied
                                newTile = occupiedTile.upTile.upTile;
                                newTile.occupied = true;
                                newPosition = newTile.transform.position;
                                newPosition.y += lift;
                                //play leap animation
                                animator.SetBool("leaping", true);
                                //go to "moving" state
                                moving = true;
                            }
                        }
                    }
                }
                
                break;

            case "left":
                if (occupiedTile.leftTile)
                {
                    //if there is a tile to the left of the occupied tile
                    if (occupiedTile.leftTile.leftTile)
                    {
                        //if the tile is not currently flipping
                        if (occupiedTile.leftTile.leftTile.isRunning == false)
                        {
                            //if the tile to the left is unoccupied
                            if (occupiedTile.leftTile.leftTile.occupied == false)
                            {
                                //new tile becomes occupied
                                newTile = occupiedTile.leftTile.leftTile;
                                newTile.occupied = true;
                                newPosition = newTile.transform.position;
                                newPosition.y += lift;
                                //play leap animation
                                animator.SetBool("leaping", true);
                                //go to "moving" state
                                moving = true;
                            }
                        }
                    }
                }
                break;

            case "down":
                if (occupiedTile.downTile)
                {
                    //if there is a tile below the occupied tile
                    if (occupiedTile.downTile.downTile)
                    {
                        //if the tile is not currently flipping
                        if (occupiedTile.downTile.downTile.isRunning == false)
                        {
                            //if the tile below is unoccupied
                            if (occupiedTile.downTile.downTile.occupied == false)
                            {
                                //new tile becomes occupied
                                newTile = occupiedTile.downTile.downTile;
                                newTile.occupied = true;
                                newPosition = newTile.transform.position;
                                newPosition.y += lift;
                                //play leap animation
                                animator.SetBool("leaping", true);
                                //go to "moving" state
                                moving = true;
                            }
                        }
                    }
                }
                break;

            case "right":
                if (occupiedTile.rightTile)
                {
                    //if there is a tile to the right of the occupied tile
                    if (occupiedTile.rightTile.rightTile)
                    {
                        //if the tile is not currently flipping
                        if (occupiedTile.rightTile.rightTile.isRunning == false)
                        {
                            //if the tile to the right is unoccupied
                            if (occupiedTile.rightTile.rightTile.occupied == false)
                            {
                                //new tile becomes occupied
                                newTile = occupiedTile.rightTile.rightTile;
                                newTile.occupied = true;
                                newPosition = newTile.transform.position;
                                newPosition.y += lift;
                                //play leap animation
                                animator.SetBool("leaping", true);
                                //go to "moving" state
                                moving = true;
                            }
                        }
                    }
                }
                break;
        }
    }

    public virtual void Ability()
    {
        //Use ability
    }
    public virtual void CloneAbility()
    {
        //Use Clone
    }
    public virtual void SwapAbility()
    {
        //Use Swap
    }

    public void Ready()
    {
        if (!ready)
        {
            ready = true;
        }
        else if (ready)
        {
            ready = false;
        }
    }



    //----------------
    // Find Functions
    //----------------

    //Find the closest tile
    public GameObject FindOccupiedTile() {
	    GameObject[] tiles;
	    GameObject occupied = null;
	    tiles = GameObject.FindGameObjectsWithTag("Tile");
	    float distance = Mathf.Infinity;
	    Vector3 position = transform.position;
	    foreach (GameObject tile in tiles) {
		    Vector3 diff = tile.transform.position - position;
		    float curDistance = diff.sqrMagnitude;
		    if (curDistance < distance) {
			    occupied = tile;
			    distance = curDistance;
		    }
	    }
	    return occupied;
	}

}
