using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapperControl : PlayerControl {


    private bool isSwapping = false;
    public int swapSpeed = 2;

    // Use this for initialization
    new void Awake () {
        base.Awake();
	}
	
	// Update is called once per frame
	void Update () {

        animator.speed = moveSpeed;

        SetDirection();

        if (onTop)
        {
            animator.SetBool("reverse", false);
        }
        else if (!onTop)
        {
            animator.SetBool("reverse", true);
        }

        //If not moving then run 'waiting' code
        if (!moving)
        {
            Waiting();
        }
        //If moving then run 'move' code
        if (moving)
        {
            Move();
        }

    }

    public override void Ability()
    {
        if (!isSwapping)
        {
            newTile = occupiedTile;
            newTile.occupied = true;
            newPosition = newTile.transform.position;
            newPosition.y -= lift;
            occupiedTile.OnTriggerExit();
            isSwapping = true;
            lift = lift * -1;
            canTurn = false;
            moving = true;
            if (onTop)
            {
                onTop = false;
            }
            else
            {
                onTop = true;
            }
            StartCoroutine(Swap());
        }
    }


    // Swap to bottom of grid
    IEnumerator Swap()
    {
        if (direction == "up")
        {
            transform.Rotate(moveSpeed * swapSpeed, 0, 0, Space.World);
            while (Swapping())
            {
                transform.Rotate(moveSpeed * swapSpeed, 0, 0, Space.World);
                yield return null;
            }
        }
        if (direction == "left")
        {
            transform.Rotate(0, 0, moveSpeed * swapSpeed, Space.World);
            while (Swapping())
            {
                transform.Rotate(0, 0, moveSpeed * swapSpeed, Space.World);
                yield return null;
            }
        }
        if (direction == "down")
        {
            transform.Rotate(-moveSpeed * swapSpeed, 0, 0, Space.World);
            while (Swapping())
            {
                transform.Rotate(-moveSpeed * swapSpeed, 0, 0, Space.World);
                yield return null;
            }
        }
        if (direction == "right")
        {
            transform.Rotate(0, 0, -moveSpeed * swapSpeed, Space.World);
            while (Swapping())
            {
                transform.Rotate(0, 0, -moveSpeed * swapSpeed, Space.World);
                yield return null;
            }
        }
    }

    //Check if tile is still flipping (hasn't reached a point where it can stop yet)
    bool Swapping()
    {

        if (transform.rotation.x != 1 && transform.rotation.x != -1 && transform.rotation.y != 1 && transform.rotation.y != -1 && transform.rotation.z != 1 && transform.rotation.z != -1 && transform.rotation.w != 1 && transform.rotation.w != -1)
        {
            return true;
        }
        else {
            isSwapping = false;
            newTile.occupied = true;
            canTurn = true;
            return false;
        }
    }
}
