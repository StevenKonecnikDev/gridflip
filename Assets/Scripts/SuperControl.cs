﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperControl : PlayerControl {

    //Swapper variables
    private bool isSwapping = false;
    private int swapSpeed = 2;

    //Cloner variables
    public bool cloned = false;
    private SuperControl cloneControl;
    private GameObject originalPlayer;
    private GameObject clone;
    private Color transparent;

    public bool disable;

    // Use this for initialization
    new void Awake()
    {
        //Initialize as a player
        base.Awake();
    }

    // Start Function
    private void Start()
    {
        //Just in case
        newTile = occupiedTile;

        if (cloned)
        {

        }
        else
        {
            originalPlayer = gameObject;
        }
    }

    // Update is called once per frame
    void Update () {

        if (this.animator.GetCurrentAnimatorStateInfo(0).IsName("jump_animation"))
        {
            moving = true;
        }
        else
        {
            animator.SetBool("jumping", false);
            newTile.occupied = true;
        }

        animator.speed = moveSpeed;

        SetDirection();

        if (onTop)
        {
            animator.SetBool("reverse", false);
        }
        else if (!onTop)
        {
            animator.SetBool("reverse", true);
        }

        //If not moving then run 'waiting' code
        if (!moving)
        {
            Waiting();
        }
        //If moving then run 'move' code
        if (moving && animator.GetBool("jumping") == false)
        {
            Move();
        }

    }

    public override void Ability()
    {
        Collider collider = gameObject.transform.GetChild(0).GetComponent<Collider>();
        if (collider.enabled == false)
        {

        }
        else
        {
            newTile = occupiedTile;
            newTile.occupied = true;
            newPosition = newTile.transform.position;
            newPosition.y += lift;
            animator.SetBool("jumping", true);
        }
    }


    public override void CloneAbility()
    {
        //Destroy the cloned player
        if (cloned)
        {
            Collider collider = gameObject.transform.GetChild(0).GetComponent<Collider>();
            if (collider.enabled == true)
            {
                occupiedTile.OnTriggerExit();
            }
            //Re-enable player script
            originalPlayer.GetComponent<PlayerControl>().enabled = true;
            //cloned = false;
            Destroy(gameObject);
        }
        //Clone the player
        else if (!cloned)
        {
            newPosition = occupiedTile.transform.position;
            newPosition.y += lift;
            //Create the clone object
            clone = (GameObject)Instantiate(originalPlayer, newPosition, transform.rotation);
            //Disable this player's control
            gameObject.GetComponent<SuperControl>().enabled = false;
            //Setup clone
            cloneControl = clone.GetComponent<SuperControl>();
            cloneControl.originalPlayer = originalPlayer;
            cloneControl.cloned = true;
            cloneControl.direction = gameObject.GetComponent<PlayerControl>().direction;
            cloneControl.newDirection = gameObject.GetComponent<PlayerControl>().direction;
            if (onTop)
            {
                cloneControl.onTop = true;
            }
            else if (!onTop)
            {
                cloneControl.onTop = false;
                cloneControl.lift = cloneControl.lift * -1;
            }

            //Make clone transparent
            transparent = clone.transform.GetChild(0).GetComponent<Renderer>().material.color;
            transparent.a = 0.5f;
            clone.transform.GetChild(0).GetComponent<Renderer>().material.color = transparent;
            //Disable the collider for the first movement
            Collider collider = clone.transform.GetChild(0).GetComponent<Collider>();
            collider.enabled = false;
        }

    }


    public override void SwapAbility()
    {
        //If not already swapping
        if (!isSwapping)
        {
            //Do the swap calculations
            newTile = occupiedTile;
            newTile.occupied = true;
            newPosition = newTile.transform.position;
            newPosition.y -= lift;
            occupiedTile.OnTriggerExit();
            isSwapping = true;
            lift = lift * -1;
            canTurn = false;
            moving = true;
            //switch onTop state
            if (onTop)
            {
                onTop = false;
            }
            else
            {
                onTop = true;
            }
            //Start the swapping coroutine
            StartCoroutine(Swap());
        }
    }


    // Swap to bottom of grid
    IEnumerator Swap()
    {
        if (direction == "up")
        {
            transform.Rotate(moveSpeed * swapSpeed, 0, 0, Space.World);
            while (Swapping())
            {
                transform.Rotate(moveSpeed * swapSpeed, 0, 0, Space.World);
                yield return null;
            }
        }
        if (direction == "down")
        {
            transform.Rotate(-moveSpeed * swapSpeed, 0, 0, Space.World);
            while (Swapping())
            {
                transform.Rotate(-moveSpeed * swapSpeed, 0, 0, Space.World);
                yield return null;
            }
        }
        if (direction == "left")
        {
            transform.Rotate(0, 0, moveSpeed * swapSpeed, Space.World);
            while (Swapping())
            {
                transform.Rotate(0, 0, moveSpeed * swapSpeed, Space.World);
                yield return null;
            }
        }
        if (direction == "right")
        {
            transform.Rotate(0, 0, -moveSpeed * swapSpeed, Space.World);
            while (Swapping())
            {
                transform.Rotate(0, 0, -moveSpeed * swapSpeed, Space.World);
                yield return null;
            }
        }
    }

    //Check if tile is still flipping (hasn't reached a point where it can stop yet)
    bool Swapping()
    {
        if(direction == "up" || direction == "down")
        {
            if (transform.rotation.x != 1 && transform.rotation.x != -1 && transform.rotation.y != 1 && transform.rotation.y != -1 && transform.rotation.z != 1 && transform.rotation.z != -1 && transform.rotation.w != 1 && transform.rotation.w != -1)
            {
                return true;
            }
            else
            {
                isSwapping = false;
                newTile.occupied = true;
                canTurn = true;
                return false;
            }
        }
        else if (direction == "left" || direction == "right")
        {
            if (transform.eulerAngles.z != 0)
            {
                return true;
            }
            else
            {
                isSwapping = false;
                newTile.occupied = true;
                canTurn = true;
                return false;
            }
        }
        else
        {
            return true;
        }


    }

}
