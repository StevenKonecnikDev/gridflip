using UnityEngine;
using System.Collections;
using TMPro;

public class KeepScore : MonoBehaviour {

    //============================
    // Variable Declaration
    //============================

    // Public Variables

    public bool gameOver = false;

	public float player1Score = 0;
	public float player2Score = 0;
    public float player3Score = 0;
    public float player4Score = 0;

    public float player1Percent = 0;
	public float player2Percent = 0;
    public float player3Percent = 0;
    public float player4Percent = 0;

    public int tilesInGrid = 0;
    public float timer = 60;
    public float countdownTime = 3;

    // Private Variables

    private TextMeshProUGUI player1Text;
    private TextMeshProUGUI player2Text;
    private TextMeshProUGUI player3Text;
    private TextMeshProUGUI player4Text;

    private TextMeshProUGUI countdownText;
    private TextMeshProUGUI timerText;
    private TextMeshProUGUI winnerText;

    private float countdownInt;
    private float timerInt;
    private float winningScore = 0;

    //holds player color/material
    private Material player1Material;
	private Material player2Material;
	private Material player3Material;
	private Material player4Material;

    private FlipTile tileScript;

    private GameObject[] scores;
    private GameObject countdownObject;
    private GameObject timerObject;
    private GameObject winnerObject;

    private GameManager gameManager;

    //============================
    // Use this for initialization
    //============================
    void Start () {

        //Get a reference to the GameManager
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        //Count all tiles in current grid
        tilesInGrid = FindNumberOfTiles();

        //Find timer, winner and score objects attached to the current grid
        countdownObject = GameObject.FindGameObjectWithTag("CountdownText");
        countdownText = countdownObject.GetComponent<TextMeshProUGUI>();
        timerObject = GameObject.FindGameObjectWithTag("TimerText");
        timerText = timerObject.GetComponent<TextMeshProUGUI>();
        winnerObject = GameObject.FindGameObjectWithTag("WinnerText");
        winnerText = winnerObject.GetComponent<TextMeshProUGUI>();

        scores = GameObject.FindGameObjectsWithTag("PlayerScore");

        foreach (GameObject score in scores)
        {
            if (score.name.Contains("1"))
            {
                player1Text = score.GetComponent<TextMeshProUGUI>();
            }
            if (score.name.Contains("2"))
            {
                player2Text = score.GetComponent<TextMeshProUGUI>();
            }
            if (score.name.Contains("3"))
            {
                player3Text = score.GetComponent<TextMeshProUGUI>();
            }
            if (score.name.Contains("4"))
            {
                player4Text = score.GetComponent<TextMeshProUGUI>();
            }
        }

        //Only enable the scores for which players are playing
        if (gameManager.player1Ready)
        {
            player1Text.enabled = true;
        }
        else
        {
            player1Text.enabled = false;
        }
        if (gameManager.player2Ready)
        {
            player2Text.enabled = true;
        }
        else
        {
            player2Text.enabled = false;
        }
        if (gameManager.player3Ready)
        {
            player3Text.enabled = true;
        }
        else
        {
            player3Text.enabled = false;
        }
        if (gameManager.player4Ready)
        {
            player4Text.enabled = true;
        }
        else
        {
            player4Text.enabled = false;
        }

        StartCoroutine("Countdown");

    }

    //================================
    // Update is called once per frame
    //================================

    void Update () {

        // Update and display scores
        UpdateScore();
        DisplayScore();

        if (!gameManager.beforeGame)
        {
            //update timer
            if (timer > 0)
            {
                timer -= Time.deltaTime;
                timerInt = Mathf.Round(timer);
                timerText.GetComponent<TextMeshProUGUI>().text = timerInt.ToString();
            }
            else if (timer <= 0)
            {
                EndGame();
            }
        }
        
    }


    //Update each player's score (can be simplified?)
    void UpdateScore()
    {
        GameObject[] tiles;
        tiles = GameObject.FindGameObjectsWithTag("Tile");
        player1Score = 0;
        player2Score = 0;
        player3Score = 0;
        player4Score = 0;
        foreach (GameObject tile in tiles)
        {
            tileScript = tile.GetComponent<FlipTile>();
            if (tileScript.topOwner == 1)
            {
                player1Score++;
            }
            if (tileScript.topOwner == 2)
            {
                player2Score++;
            }
            if (tileScript.topOwner == 3)
            {
                player3Score++;
            }
            if (tileScript.topOwner == 4)
            {
                player4Score++;
            }
        }
    }

    //Convert scores to percentages and display
    void DisplayScore()
    {
        //convert score to percent of grid owned
        player1Percent = Mathf.Round(player1Score / tilesInGrid * 100);
        player2Percent = Mathf.Round(player2Score / tilesInGrid * 100);
        player3Percent = Mathf.Round(player3Score / tilesInGrid * 100);
        player4Percent = Mathf.Round(player4Score / tilesInGrid * 100);
        //display percent on screen
        player1Text.GetComponent<TextMeshProUGUI>().text = player1Percent.ToString() + "%";
        player2Text.GetComponent<TextMeshProUGUI>().text = player2Percent.ToString() + "%";
        player3Text.GetComponent<TextMeshProUGUI>().text = player3Percent.ToString() + "%";
        player4Text.GetComponent<TextMeshProUGUI>().text = player4Percent.ToString() + "%";
    }

    //End game calculations and displays
    void EndGame()
    {
        gameOver = true;
        winnerText.enabled = true;

        winningScore = Mathf.Max(player1Percent, player2Percent, player3Percent, player4Percent);

        //Player 1 wins
        if (winningScore == player1Percent)
        {
            winnerText.GetComponent<TextMeshProUGUI>().text = "Player 1\nWINS";
            winnerText.GetComponent<TextMeshProUGUI>().faceColor = Color.red;
        }
        //Player 2 wins
        if (winningScore == player2Percent)
        {
            winnerText.GetComponent<TextMeshProUGUI>().text = "Player 2\nWINS";
            winnerText.GetComponent<TextMeshProUGUI>().faceColor = Color.blue;
        }
        //Player 3 wins
        if (winningScore == player3Percent)
        {
            winnerText.GetComponent<TextMeshProUGUI>().text = "Player 3\nWINS";
            winnerText.GetComponent<TextMeshProUGUI>().faceColor = Color.green;
        }
        //Player 4 wins
        if (winningScore == player4Percent)
        {
            winnerText.GetComponent<TextMeshProUGUI>().text = "Player 4\nWINS";
            winnerText.GetComponent<TextMeshProUGUI>().faceColor = Color.yellow;
        }

        Time.timeScale = 0;
    }


    IEnumerator Countdown()
    {
        gameManager.beforeGame = true;
        //Time.timeScale = 0;
        while (countdownTime > 1)
        {
            countdownTime -= Time.deltaTime;
            countdownInt = Mathf.Round(countdownTime);
            countdownText.GetComponent<TextMeshProUGUI>().text = countdownInt.ToString();
            yield return null;
        }
        countdownText.enabled = false;
        gameManager.beforeGame = false;
    }


    //===============
    // Find Functions
    //===============

    //Find the total number of tiles in the grid
    int FindNumberOfTiles()
    {
        GameObject[] tiles;
        tiles = GameObject.FindGameObjectsWithTag("Tile");
        int amount = 0;
        foreach (GameObject tile in tiles)
        {
            amount++;
        }
        return amount;
    }
}
