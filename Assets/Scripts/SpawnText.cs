﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpawnText : MonoBehaviour {

    private GameManager gameManager;
    private TextMeshProUGUI TextPro;
    private int playerNumber = 0;

    // Use this for initialization
    void Start () {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        TextPro = GetComponent<TextMeshProUGUI>();

        //If the text is for player 1
        if (gameObject.name.Contains("1"))
        {
            playerNumber = 1;
        }
        else if (gameObject.name.Contains("2"))
        {
            playerNumber = 2;
        }
        else if (gameObject.name.Contains("3"))
        {
            playerNumber = 3;
        }
        else if (gameObject.name.Contains("4"))
        {
            playerNumber = 4;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (playerNumber == 1)
        {
            //If player 1 exists
            if (gameManager.player1)
            {
                if (gameManager.player1Ready)
                {
                    TextPro.text = "Player 1 Ready!";
                }
                else
                {
                    TextPro.text = "Press start";
                }
            }
            else
            {
                TextPro.text = "Press Y to play";
            }
        }

        if (playerNumber == 2)
        {
            //If player 2 exists
            if (gameManager.player2)
            {
                if (gameManager.player2Ready)
                {
                    TextPro.text = "Player 2 Ready!";
                }
                else
                {
                    TextPro.text = "Press start";
                }
            }
            else
            {
                TextPro.text = "Press Y to play";
            }
        }

        if (playerNumber == 3)
        {
            //If player 3 exists
            if (gameManager.player3)
            {
                if (gameManager.player3Ready)
                {
                    TextPro.text = "Player 3 Ready!";
                }
                else
                {
                    TextPro.text = "Press start";
                }
            }
            else
            {
                TextPro.text = "Press Y to play";
            }
        }

        if (playerNumber == 4)
        {
            //If player 4 exists
            if (gameManager.player4)
            {
                if (gameManager.player4Ready)
                {
                    TextPro.text = "Player 4 Ready!";
                }
                else
                {
                    TextPro.text = "Press start";
                }
            }
            else
            {
                TextPro.text = "Press Y to play";
            }
        }

    }
}
