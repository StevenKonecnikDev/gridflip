using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SceneText : MonoBehaviour {

    private GameManager gameManager;
    private TextMeshProUGUI TextPro;

    // Use this for initialization
    void Start () {

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        TextPro = GetComponent<TextMeshProUGUI>();

    }
	
	// Update is called once per frame
	void Update () {

        TextPro.text = gameManager.sceneName;
    }
}
