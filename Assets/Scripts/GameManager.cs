using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;
using Rewired;

public class GameManager : MonoBehaviour {

    //============================
    // Variable Declaration
    //============================

    // Public Variables

    public bool player1Ready = false;
    public bool player2Ready = false;
    public bool player3Ready = false;
    public bool player4Ready = false;

    public GameObject token1;
    public GameObject token2;
    public GameObject token3;
    public GameObject token4;

    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;

    public Vector3 Player1pos = new Vector3(-2.5f, 0.1f, 2.5f);
    public Vector3 Player2pos = new Vector3(2.5f, 0.1f, 2.5f);
    public Vector3 Player3pos = new Vector3(-2.5f, 0.1f, -2.5f);
    public Vector3 Player4pos = new Vector3(2.5f, 0.1f, -2.5f);

    public int playerSpeed = 3;
    public int minSpeed = 1;
    public int maxSpeed = 5;
    public bool abilitiesActivated = false;
    public bool demo = false;
    public string sceneName = "Demo";
    public bool beforeGame = false;

    // Private Variables

    private Player ControllerAdmin;
    private Player Controller1;
    private Player Controller2;
    private Player Controller3;
    private Player Controller4;

    private Material player1Material;
    private Material player2Material;
    private Material player3Material;
    private Material player4Material;

    private PlayerControl player1Control;
    private PlayerControl player2Control;
    private PlayerControl player3Control;
    private PlayerControl player4Control;

    private string[] sceneNames;
    private int sceneNamesIndex = 0;
    private GameObject pauseObject;
    private TextMeshProUGUI pauseText;

    private Scene thisScene;

    private bool playersEnabled = false;

    

    //============================
    // Use this for initialization
    //============================

    void Start () {

        Time.timeScale = 1;

        //Get a list of scenes in build settings because Unity can't
        sceneNames = new string[] { "Demo", "Grid Small", "Grid Medium", "Grid Large"};

        //Get reference to the current scene and the current scene build index
        thisScene = SceneManager.GetActiveScene();

        //Get contorller references for admin and players
        ControllerAdmin = ReInput.players.GetPlayer("SYSTEM");

        Controller1 = ReInput.players.GetPlayer(0);
        Controller2 = ReInput.players.GetPlayer(1);
        Controller3 = ReInput.players.GetPlayer(2);
        Controller4 = ReInput.players.GetPlayer(3);

        //If Demo scene set demo flag as true else clear demo flag
        if (thisScene.name.Contains("Demo"))
        {
            demo = true;
        }
        else
        {
            demo = false;
        }

        //Necessary code for OnSceneLoaded function
        SceneManager.sceneLoaded += OnSceneLoaded;

    }

    //When any scene is loaded
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Time.timeScale = 1;

        playersEnabled = false;

        //Get reference to the current scene and the current scene build index
        thisScene = SceneManager.GetActiveScene();

        //If Demo scene set demo flag as true else clear demo flag
        if (thisScene.name.Contains("Demo"))
        {
            demo = true;
            //Clear Player ready bools
            player1Ready = false;
            player2Ready = false;
            player3Ready = false;
            player4Ready = false;
            //Collect the set player start positions
            Player1pos = GameObject.Find("Player1Pos").transform.position;
            Player2pos = GameObject.Find("Player2Pos").transform.position;
            Player3pos = GameObject.Find("Player3Pos").transform.position;
            Player4pos = GameObject.Find("Player4Pos").transform.position;
        }
        else
        {
            demo = false;
        }

        //If this scene is a Grid
        if (thisScene.name.Contains("Grid"))
        {
            //Set PauseText
            pauseObject = GameObject.FindGameObjectWithTag("PauseText");
            pauseText = pauseObject.GetComponent<TextMeshProUGUI>();

            //Collect the set player start positions
            Player1pos = GameObject.Find("Player1Pos").transform.position;
            Player2pos = GameObject.Find("Player2Pos").transform.position;
            Player3pos = GameObject.Find("Player3Pos").transform.position;
            Player4pos = GameObject.Find("Player4Pos").transform.position;

            //Checks which players have were ready to play
            if (player1Ready)
            {
                Instantiate(token1, Player1pos, Quaternion.identity);
                player1 = GameObject.Find("Player1");
                player1Control = player1.GetComponent<SuperControl>();
                player1Control.enabled = false;
            }
            if (player2Ready)
            {
                Instantiate(token2, Player2pos, Quaternion.identity);
                player2 = GameObject.Find("Player2");
                player2Control = player2.GetComponent<SuperControl>();
                player2Control.enabled = false;
            }
            if (player3Ready)
            {
                Instantiate(token3, Player3pos, Quaternion.identity);
                player3 = GameObject.Find("Player3");
                player3Control = player3.GetComponent<SuperControl>();
                player3Control.enabled = false;
            }
            if (player4Ready)
            {
                Instantiate(token4, Player4pos, Quaternion.identity);
                player4 = GameObject.Find("Player4");
                player4Control = player4.GetComponent<SuperControl>();
                player4Control.enabled = false;
            }


        }
    }

    //================================
    // Update is called once per frame
    //================================

    void Update () {

        //If Demo scene allow player spawning and selecting Grid
        if (demo)
        {
            //Allows these functions to run if in Demo scene
            Spawnable();
            ChooseGrid();
            ChangeSpeed();
            CheckReady();
            ToggleAbilities();
        }
        //If not in demo scene enable these functions
        else
        {
            EnablePlayers(); //Runs only once
            PauseControls();
        }

        //Allows admin controls
        AdminControls();
        
    }

    //Debug and Demo keyboard controls
    void AdminControls()
    {
        if (ControllerAdmin.GetButtonDown("Escape"))
        {
            Debug.Log("Quit Game");
            Application.Quit();
        }
        if (ControllerAdmin.GetButtonDown("Restart"))
        {
            Debug.Log("Restart Level");
            SceneManager.LoadScene(thisScene.name);
        }
        if (ControllerAdmin.GetButtonDown("Return"))
        {
            Debug.Log("Return");
            SceneManager.LoadScene("Demo");
        }
    }

    void EnablePlayers()
    {
        if (!beforeGame && !playersEnabled)
        {
            if (player1Ready)
            {
                player1Control.enabled = true;
            }
            if (player2Ready)
            {
                player2Control.enabled = true;
            }
            if (player3Ready)
            {
                player3Control.enabled = true;
            }
            if (player4Ready)
            {
                player4Control.enabled = true;
            }
            playersEnabled = true;
        }
    }

    void PauseControls()
    {
        if (Controller1.GetButtonDown("Start"))
        {
            if(Time.timeScale == 1)
            {
                pauseText.GetComponent<TextMeshProUGUI>().text = "P1 PAUSED";
                pauseText.enabled = true;
                Time.timeScale = 0;
            }
            else
            {
                pauseText.enabled = false;
                Time.timeScale = 1;
            }
        }
        if (Controller2.GetButtonDown("Start"))
        {
            if (Time.timeScale == 1)
            {
                pauseText.GetComponent<TextMeshProUGUI>().text = "P2 PAUSED";
                pauseText.enabled = true;
                Time.timeScale = 0;
            }
            else
            {
                pauseText.enabled = false;
                Time.timeScale = 1;
            }
        }
        if (Controller3.GetButtonDown("Start"))
        {
            if (Time.timeScale == 1)
            {
                pauseText.GetComponent<TextMeshProUGUI>().text = "P3 PAUSED";
                pauseText.enabled = true;
                Time.timeScale = 0;
            }
            else
            {
                pauseText.enabled = false;
                Time.timeScale = 1;
            }
        }
        if (Controller4.GetButtonDown("Start"))
        {
            if (Time.timeScale == 1)
            {
                pauseText.GetComponent<TextMeshProUGUI>().text = "P4 PAUSED";
                pauseText.enabled = true;
                Time.timeScale = 0;
            }
            else
            {
                pauseText.enabled = false;
                Time.timeScale = 1;
            }
        }
    }

    //=================
    // Functions
    //=================

    //Allows players to spawn themselves and remove themselves from play
    void Spawnable()
    {
        //If player hits spawn button
        if (Controller1.GetButtonDown("Spawn"))
        {
            //If player does not exist
            if (!player1)
            {
                //Create player in position
                Instantiate(token1, Player1pos, Quaternion.identity);
                player1 = GameObject.Find("Player1");
                player1Control = player1.GetComponent<SuperControl>();
            }
            //If player already exists
            else if (player1)
            {
                player1Ready = false;
                //Set flag in player control to remove player
                player1Control.killMe = true;
            }
        }
        //If player hits spawn button
        if (Controller2.GetButtonDown("Spawn"))
        {
            //If player does not exist
            if (!player2)
            {
                //Create player in position
                Instantiate(token2, Player2pos, Quaternion.identity);
                player2 = GameObject.Find("Player2");
                player2Control = player2.GetComponent<SuperControl>();
            }
            //If player already exists
            else if (player2)
            {
                player2Ready = false;
                //Set flag in player control to remove player
                player2Control.killMe = true;
            }
        }
        //If player hits spawn button
        if (Controller3.GetButtonDown("Spawn"))
        {
            //If player does not exist
            if (!player3)
            {
                //Create player in position
                Instantiate(token3, Player3pos, Quaternion.identity);
                player3 = GameObject.Find("Player3");
                player3Control = player3.GetComponent<SuperControl>();
            }
            //If player already exists
            else if (player3)
            {
                player3Ready = false;
                //Set flag in player control to remove player
                player3Control.killMe = true;
            }
        }
        //If player hits spawn button
        if (Controller4.GetButtonDown("Spawn"))
        {
            //If player does not exist
            if (!player4)
            {
                //Create player in position
                Instantiate(token4, Player4pos, Quaternion.identity);
                player4 = GameObject.Find("Player4");
                player4Control = player4.GetComponent<SuperControl>();
            }
            //If player already exists
            else if (player4)
            {
                player4Ready = false;
                //Set flag in player control to remove player
                player4Control.killMe = true;
            }
        }
    }

    //Allows scene selection from Demo scene
    void ChooseGrid()
    {
        if (ControllerAdmin.GetButtonDown("Next Grid"))
        {
            if (sceneNamesIndex < sceneNames.Length - 1)
            {
                sceneNamesIndex += 1;
            }
        }
        if (ControllerAdmin.GetButtonDown("Previous Grid"))
        {
            if (sceneNamesIndex > 0)
            {
                sceneNamesIndex -= 1;
            }
        }
        sceneName = sceneNames[sceneNamesIndex];
    }

    //Allows scene selection from Demo scene
    void ChangeSpeed()
    {
        if (ControllerAdmin.GetButtonDown("Speed Up"))
            if (playerSpeed < maxSpeed)
            {
                playerSpeed += 1;
            }
        if (ControllerAdmin.GetButtonDown("Speed Down"))
        {
            if (playerSpeed > minSpeed)
            {
                playerSpeed -= 1;
            }
        }
        if (player1)
        {
            player1Control.moveSpeed = playerSpeed;
        }
        if (player2)
        {
            player2Control.moveSpeed = playerSpeed;
        }
        if (player3)
        {
            player3Control.moveSpeed = playerSpeed;
        }
        if (player4)
        {
            player4Control.moveSpeed = playerSpeed;
        }
    }

    //Function to check which players have selected ready and how many
    void CheckReady()
    {
        int number = 0;
        if (player1)
        {
            if (player1Control.ready)
            {
                player1Ready = true;
                number++;
            }
            else
            {
                player1Ready = false;
            }
        }
        if (player2)
        {
            if (player2Control.ready)
            {
                player2Ready = true;
                number++;
            }
            else
            {
                player2Ready = false;
            }
        }
        if (player3)
        {
            if (player3Control.ready)
            {
                player3Ready = true;
                number++;
            }
            else
            {
                player3Ready = false;
            }
        }
        if (player4)
        {
            if (player4Control.ready)
            {
                player4Ready = true;
                number++;
            }
            else
            {
                player4Ready = false;
            }
        }

        //If more than one player is ready load the chosen scene
        if (number > 1)
        {
            int activePlayers = CountPlayers();
            if (number == activePlayers)
            {
                SceneManager.LoadScene(sceneName);
            }
        }

    }

    //Allow admin keybaord to toggle player abilities on/off
    void ToggleAbilities()
    {
        if (ControllerAdmin.GetButtonDown("Toggle Abilities"))
        {
            if (abilitiesActivated)
            {
                abilitiesActivated = false;
            }
            else if (!abilitiesActivated)
            {
                abilitiesActivated = true;
            }
        }
    }

    //=================
    // Return Functions
    //=================

    //Returns the total number of current players
    int CountPlayers()
    {
        int number = 0;
        if (player1)
        {
            number++;
        }
        if (player2)
        {
            number++;
        }
        if (player3)
        {
            number++;
        }
        if (player4)
        {
            number++;
        }
        return number;
    }


}
