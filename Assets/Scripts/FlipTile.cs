using UnityEngine;
using System.Collections;

public class FlipTile : MonoBehaviour {

	//========
	// Globals
	//========

	public float flipSpeed = 5.0f;
	public bool occupied = false;
	public int topOwner = 0;
	public int oldOwner = 0;
	public int bottomOwner = 0;

    //adjacent tile bools
    public bool tileUp = false;
	public bool tileLeft = false;
	public bool tileDown = false;
	public bool tileRight = false;

	//reference to adjacent tiles
	public FlipTile upTile;
	public FlipTile leftTile;
	public FlipTile downTile;
	public FlipTile rightTile;

    //if cooroutine is already running
    public bool isRunning = false;

    //==================
    // Private Variables
    //==================

    //positions one unit from the occupied tile
    private Vector3 upPosition;
	private Vector3 leftPosition;
	private Vector3 downPosition;
	private Vector3 rightPosition;

    private float tileSpace = 1.0f;

	//check radius of sphere
	private float checkRadius = 0.1f;
	//direction in which the flip should occur
	private string flipDirection = "stationary";
	
    //holds player color/material
    private Material player1Material;
	private Material player2Material;
	private Material player3Material;
	private Material player4Material;
	

	//reference to current player on tile
	private PlayerControl currentPlayer;

	//nessessary quaternions
	private Quaternion negativeIndentity;

    private GameManager gameManager;


    //===============
    // Initialization
    //===============

    void Start () {

        //Get a reference to the GameManager
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        //Set the flip speed to be twice the speed of the players
        flipSpeed = gameManager.playerSpeed * 2;

        negativeIndentity = Quaternion.identity;
		negativeIndentity.w = -1.0f;

		//Grab Player colors
		player1Material = Resources.Load("Materials/Player1Tile", typeof(Material)) as Material;
		player2Material = Resources.Load("Materials/Player2Tile", typeof(Material)) as Material;
		player3Material = Resources.Load("Materials/Player3Tile", typeof(Material)) as Material;
		player4Material = Resources.Load("Materials/Player4Tile", typeof(Material)) as Material;


		//Check for adjacent tiles
		upPosition = transform.position;
		upPosition.z += tileSpace;
		leftPosition = transform.position;
		leftPosition.x -= tileSpace;
		downPosition = transform.position;
		downPosition.z -= tileSpace;
		rightPosition = transform.position;
		rightPosition.x += tileSpace;

		if (Physics.CheckSphere(upPosition, checkRadius)){
			tileUp = true;
			upTile = FindClosestTile(upPosition).GetComponent<FlipTile>();
		}
		if (Physics.CheckSphere(leftPosition, checkRadius)){
			tileLeft = true;
			leftTile = FindClosestTile(leftPosition).GetComponent<FlipTile>();
		}
		if (Physics.CheckSphere(downPosition, checkRadius)){
			tileDown = true;
			downTile = FindClosestTile(downPosition).GetComponent<FlipTile>();
		}
		if (Physics.CheckSphere(rightPosition, checkRadius)){
			tileRight = true;
			rightTile = FindClosestTile(rightPosition).GetComponent<FlipTile>();
		}

	}
	
	//-----------------
	// Main Update Loop
	//-----------------

	void Update () {

        //Set the flip speed to be twice the speed of the players
        flipSpeed = gameManager.playerSpeed * 2;
    }

	//===============
	// Trigger Events
	//===============

	//When player steps on a tile it turns to their color
	public void OnTriggerEnter () {

		//occupied = true;
		currentPlayer = FindCurrentPlayer ().GetComponent<PlayerControl> ();
        foreach (Transform child in transform)
        {
            //If the player is on the top of the grid
            if (currentPlayer.onTop)
            {
                //Change color of upper side of tile
                if (child.position.y > transform.position.y)
                {
                    if (currentPlayer.name == "Player1")
                    {
                        child.GetComponent<Renderer>().material = player1Material;
                        topOwner = 1;
                    }
                    if (currentPlayer.name == "Player2")
                    {
                        child.GetComponent<Renderer>().material = player2Material;
                        topOwner = 2;
                    }
                    if (currentPlayer.name == "Player3")
                    {
                        child.GetComponent<Renderer>().material = player3Material;
                        topOwner = 3;
                    }
                    if (currentPlayer.name == "Player4")
                    {
                        child.GetComponent<Renderer>().material = player4Material;
                        topOwner = 4;
                    }
                }
            }
            //If the player is on the bottom of the grid
            else if (!currentPlayer.onTop)
            {
                //Change color of lower side of tile
                if (child.position.y < transform.position.y)
                {
                    if (currentPlayer.name == "Player1")
                    {
                        child.GetComponent<Renderer>().material = player1Material;
                        bottomOwner = 1;
                    }
                    if (currentPlayer.name == "Player2")
                    {
                        child.GetComponent<Renderer>().material = player2Material;
                        bottomOwner = 2;
                    }
                    if (currentPlayer.name == "Player3")
                    {
                        child.GetComponent<Renderer>().material = player3Material;
                        bottomOwner = 3;
                    }
                    if (currentPlayer.name == "Player4")
                    {
                        child.GetComponent<Renderer>().material = player4Material;
                        bottomOwner = 4;
                    }
                }
            }
		}
	}

	//When player steps off a tile the tile flips in the direction of the player
	public void OnTriggerExit () {

        currentPlayer = FindCurrentPlayer().GetComponent<PlayerControl>();

        if (currentPlayer.onTop)
        {
            oldOwner = topOwner;
            topOwner = bottomOwner;
            bottomOwner = oldOwner;
        }
        else if (!currentPlayer.onTop)
        {
            oldOwner = bottomOwner;
            bottomOwner = topOwner;
            topOwner = oldOwner;
        }


        if (!isRunning){
			//get the direction to flip in
			flipDirection = currentPlayer.direction;
            //start flip coroutine
            isRunning = true;
            StartCoroutine(Flip());
		}
	}

	//===========
	// Coroutines
	//===========

    // Flip Tile
	IEnumerator Flip(){

		if (flipDirection == "up")
		{
			transform.Rotate(flipSpeed,0,0, Space.World);
			while (Flipping())
			{
				transform.Rotate(flipSpeed,0,0, Space.World);
				yield return null;
			}
		}
		if (flipDirection == "left")
		{
			transform.Rotate(0,0,flipSpeed, Space.World);
			while (Flipping())
			{
				transform.Rotate(0,0,flipSpeed, Space.World);
				yield return null;
			}
		}
		if (flipDirection == "down")
		{
			transform.Rotate(-flipSpeed,0,0, Space.World);
			while (Flipping())
			{
				transform.Rotate(-flipSpeed,0,0, Space.World);
				yield return null;
			}
		}
		if (flipDirection == "right")
		{
            transform.Rotate(0,0,-flipSpeed, Space.World);
			while (Flipping())
			{
				transform.Rotate(0,0,-flipSpeed, Space.World);
				yield return null;
			}
		}
	}

	//Check if tile is still flipping (hasn't reached a point where it can stop yet)
	bool Flipping() {
        if (transform.rotation.x != 1 && transform.rotation.x != -1 && transform.rotation.y != 1 && transform.rotation.y != -1 && transform.rotation.z != 1 && transform.rotation.z != -1 && transform.rotation.w != 1 && transform.rotation.w != -1)
        {
            return true;
		}
		else{
			isRunning = false;
            occupied = false;
			return false;
		}
	}


	//===============
	// Find Functions
	//===============

    //Find the closest tile to a given point
	GameObject FindClosestTile(Vector3 directionPosition) {
		GameObject[] tiles;
		GameObject closest = null;
		tiles = GameObject.FindGameObjectsWithTag("Tile");
		float distance = Mathf.Infinity;
		Vector3 position = directionPosition;
		foreach (GameObject tile in tiles) {
			Vector3 diff = tile.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = tile;
				distance = curDistance;
			}
		}
		return closest;
	}

	//Find the player that is currently exiting the tile
	GameObject FindCurrentPlayer() {
		GameObject[] players;
		GameObject current = null;
		players = GameObject.FindGameObjectsWithTag("Player");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject player in players) {
			Vector3 diff = player.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				current = player;
				distance = curDistance;
			}
		}
		return current;
	}


}
